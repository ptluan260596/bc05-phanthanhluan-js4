document.getElementById("btnKetQua").onclick = function () {
  var thang = document.getElementById("thang").value * 1;
  var nam = document.getElementById("nam").value * 1;
  if (
    thang == 1 ||
    thang == 3 ||
    thang == 5 ||
    thang == 7 ||
    thang == 8 ||
    thang == 10 ||
    thang == 12
  ) {
    document.getElementById("ketQua").innerHTML =
      "Tháng " + thang + " của Năm " + nam + " có 31 ngày";
  } else if (thang == 4 || thang == 6 || thang == 9 || thang == 11) {
    document.getElementById("ketQua").innerHTML =
      "Tháng " + thang + " của Năm " + nam + " có 30 ngày";
  } else if (
    (thang == 2 && nam % 4 === 0 && nam % 100 !== 0 && nam % 400 !== 0) ||
    (thang == 2 && nam % 100 === 0 && nam % 400 === 0)
  ) {
    document.getElementById("ketQua").innerHTML =
      "Tháng " + thang + " của Năm " + nam + " có 29 ngày";
  } else {
    document.getElementById("ketQua").innerHTML =
      "Tháng " + thang + " của Năm " + nam + " có 28 ngày";
  }
};
