document.getElementById("btnYesterday").onclick = function () {
  var ngay = document.getElementById("day").value * 1;
  var thang = document.getElementById("month").value * 1;
  var nam = document.getElementById("year").value * 1;
  if (
    (ngay == 1 && thang == 5) ||
    (ngay == 1 && thang == 7) ||
    (ngay == 1 && thang == 10) ||
    (ngay == 1 && thang == 12)
  ) {
    thang = thang - 1;
    document.getElementById("yesterday").innerHTML =
      "30" + "/" + thang + "/" + nam;
  } else if (
    (ngay == 1 && thang == 2) ||
    (ngay == 1 && thang == 4) ||
    (ngay == 1 && thang == 6) ||
    (ngay == 1 && thang == 8) ||
    (ngay == 1 && thang == 9) ||
    (ngay == 1 && thang == 11)
  ) {
    thang = thang - 1;
    document.getElementById("yesterday").innerHTML =
      "31" + "/" + thang + "/" + nam;
  } else if (ngay == 1 && thang == 3) {
    if (
      (nam % 4 === 0 && nam % 100 !== 0 && nam % 400 !== 0) ||
      (nam % 100 === 0 && nam % 400 === 0)
    ) {
      thang = thang - 1;
      document.getElementById("yesterday").innerHTML =
        "29" + "/" + thang + "/" + nam;
    } else {
      thang = thang - 1;
      document.getElementById("yesterday").innerHTML =
        "28" + "/" + thang + "/" + nam;
    }
  } else if (ngay == 1 && thang == 1) {
    nam = nam - 1;
    document.getElementById("yesterday").innerHTML =
      "31" + "/" + "12" + "/" + nam;
  } else {
    ngay = ngay - 1;
    document.getElementById("yesterday").innerHTML =
      ngay + "/" + thang + "/" + nam;
  }
};
document.getElementById("btnTomorrow").onclick = function () {
  var ngay = document.getElementById("day").value * 1;
  var thang = document.getElementById("month").value * 1;
  var nam = document.getElementById("year").value * 1;
  if (
    (ngay == 31 && thang == 1) ||
    (ngay == 31 && thang == 3) ||
    (ngay == 31 && thang == 5) ||
    (ngay == 31 && thang == 7) ||
    (ngay == 31 && thang == 8) ||
    (ngay == 31 && thang == 10) ||
    (ngay == 31 && thang == 12)
  ) {
    thang = thang + 1;
    document.getElementById("tomorrow").innerHTML =
      "1" + "/" + thang + "/" + nam;
  } else if (
    (ngay == 30 && thang == 4) ||
    (ngay == 30 && thang == 6) ||
    (ngay == 30 && thang == 9) ||
    (ngay == 30 && thang == 11)
  ) {
    thang = thang + 1;
    document.getElementById("tomorrow").innerHTML =
      "1" + "/" + thang + "/" + nam;
  } else if (
    (thang == 2 &&
      ngay == 29 &&
      nam % 4 === 0 &&
      nam % 100 !== 0 &&
      nam % 400 !== 0) ||
    (thang == 2 && ngay == 29 && nam % 100 === 0 && nam % 400 === 0)
  ) {
    thang = thang + 1;
    document.getElementById("tomorrow").innerHTML =
      "1" + "/" + thang + "/" + nam;
  } else if (
    (thang == 2 &&
      ngay == 28 &&
      nam % 4 === 0 &&
      nam % 100 !== 0 &&
      nam % 400 !== 0) ||
    (thang == 2 && ngay == 29 && nam % 100 === 0 && nam % 400 === 0)
  ) {
    ngay = ngay + 1;
    document.getElementById("tomorrow").innerHTML =
      ngay + "/" + thang + "/" + nam;
  } else if (ngay == 31 && thang == 12) {
    nam = nam + 1;
    document.getElementById("tomorrow").innerHTML = "1" + "/" + "1" + "/" + nam;
  } else {
    ngay = ngay + 1;
    document.getElementById("tomorrow").innerHTML =
      ngay + "/" + thang + "/" + nam;
  }
};
