document.getElementById("btnDoc").onclick = function () {
  var so = document.getElementById("so").value * 1;
  var tram = Math.floor(so / 100);
  var chuc = Math.floor((so % 100) / 10);
  var donVi = so % 10;
  if (tram == 1) {
    tram = "một trăm";
  } else if (tram == 2) {
    tram = "hai trăm";
  } else if (tram == 3) {
    tram = "ba trăm";
  } else if (tram == 4) {
    tram = "bốn trăm";
  } else if (tram == 5) {
    tram = "năm trăm";
  } else if (tram == 6) {
    tram = "sáu trăm";
  } else if (tram == 7) {
    tram = "bảy trăm";
  } else if (tram == 8) {
    tram = "tám trăm";
  } else if (tram == 9) {
    tram = "chín trăm";
  }
  if (chuc == 1) {
    chuc = "mười";
  } else if (chuc == 2) {
    chuc = "hai mươi";
  } else if (chuc == 3) {
    chuc = "ba mươi";
  } else if (chuc == 4) {
    chuc = "bốn mươi";
  } else if (chuc == 5) {
    chuc = "năm mươi";
  } else if (chuc == 6) {
    chuc = "sáu mươi";
  } else if (chuc == 7) {
    chuc = "bảy mươi";
  } else if (chuc == 8) {
    chuc = "tám mươi";
  } else if (chuc == 9) {
    chuc = "chín mươi";
  }
  if (donVi == 1) {
    donVi = "một";
  } else if (donVi == 2) {
    donVi = "hai";
  } else if (donVi == 3) {
    donVi = "ba";
  } else if (donVi == 4) {
    donVi = "bốn";
  } else if (donVi == 5) {
    donVi = "năm";
  } else if (donVi == 6) {
    donVi = "sáu";
  } else if (donVi == 7) {
    donVi = "bảy";
  } else if (donVi == 8) {
    donVi = "tám";
  } else if (donVi == 9) {
    donVi = "chín";
  }
  if (chuc == 0 && donVi == 0) {
    document.getElementById("ketQua").innerHTML = tram;
  } else if (donVi == 0) {
    document.getElementById("ketQua").innerHTML = tram + " " + chuc;
  } else if (chuc == 0) {
    document.getElementById("ketQua").innerHTML = `${tram} lẻ ${donVi}`;
  } else {
    document.getElementById("ketQua").innerHTML = `${tram} ${chuc} ${donVi}`;
  }
};
